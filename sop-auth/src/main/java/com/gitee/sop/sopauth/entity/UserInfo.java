package com.gitee.sop.sopauth.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.sop.sopauth.auth.OpenUser;
import lombok.Data;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.Table;
import java.util.Date;


/**
 * 表名：user_info
 * 备注：用户信息表
 *
 * @author tanghc
 */
@Table(name = "user_info",pk = @Pk(name = "id"))
@Data
public class UserInfo implements OpenUser {
    /**  数据库字段：id */
    private Long id;

    /** 用户名, 数据库字段：username */
    private String username;

    /** 密码, 数据库字段：password */
    @JSONField(serialize = false)
    private String password;

    /** 昵称, 数据库字段：nickname */
    private String nickname;

    /**  数据库字段：gmt_create */
    private Date gmtCreate;

    /**  数据库字段：gmt_modified */
    private Date gmtModified;

    @Override
    public String getUserId() {
        return String.valueOf(id);
    }
}
