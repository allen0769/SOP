package com.gitee.sop.sopauth.entity;

import lombok.Data;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.Table;


/**
 * 表名：isv_info
 * 备注：isv信息表
 *
 * @author tanghc
 */
@Table(name = "isv_info",pk = @Pk(name = "id"))
@Data
public class IsvInfo {
    /**  数据库字段：id */
    private Long id;

    /** appKey, 数据库字段：app_key */
    private String appKey;

    /** 1启用，2禁用, 数据库字段：status */
    private Byte status;
}
