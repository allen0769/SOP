package com.gitee.sop.adminserver.entity;

import lombok.Data;

import java.util.Date;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.Table;


/**
 * 表名：perm_role_permission
 * 备注：角色权限表
 *
 * @author tanghc
 */
@Table(name = "perm_role_permission",pk = @Pk(name = "id"))
@Data
public class PermRolePermission {
    /**  数据库字段：id */
    private Long id;

    /** 角色表code, 数据库字段：role_code */
    private String roleCode;

    /** api_id, 数据库字段：route_id */
    private String routeId;

    /**  数据库字段：gmt_create */
    private Date gmtCreate;

    /**  数据库字段：gmt_modified */
    private Date gmtModified;
}
