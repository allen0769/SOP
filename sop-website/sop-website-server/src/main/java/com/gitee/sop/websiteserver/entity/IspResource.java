package com.gitee.sop.websiteserver.entity;

import lombok.Data;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.Table;
import java.util.Date;


/**
 * 表名：isp_resource
 * 备注：ISP资源表
 *
 * @author tanghc
 */
@Table(name = "isp_resource",pk = @Pk(name = "id"))
@Data
public class IspResource {
    /**  数据库字段：id */
    private Long id;

    /** 资源名称, 数据库字段：name */
    private String name;

    private String version;

    /** 资源内容（URL）, 数据库字段：content */
    private String content;

    private String extContent;

    /** 资源类型：0：SDK链接, 数据库字段：type */
    private Byte type;

    /**  数据库字段：is_deleted */
    @com.gitee.fastmybatis.core.annotation.LogicDelete
    private Byte isDeleted;

    /**  数据库字段：gmt_create */
    private Date gmtCreate;

    /**  数据库字段：gmt_modified */
    private Date gmtModified;
}
