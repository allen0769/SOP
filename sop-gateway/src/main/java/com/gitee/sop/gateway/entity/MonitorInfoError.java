package com.gitee.sop.gateway.entity;

import lombok.Data;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.Table;
import java.time.LocalDateTime;
import java.util.Date;


/**
 * 表名：monitor_info_error
 *
 * @author tanghc
 */
@Table(name = "monitor_info_error",pk = @Pk(name = "id"))
@Data
public class MonitorInfoError {
    /**  数据库字段：id */
    private Long id;

    /** 错误id,md5Hex(instanceId + routeId + errorMsg), 数据库字段：error_id */
    private String errorId;

    /** 实例id, 数据库字段：instance_id */
    private String instanceId;

    /**  数据库字段：route_id */
    private String routeId;

    /**  数据库字段：error_msg */
    private String errorMsg;

    /** http status，非200错误, 数据库字段：error_status */
    private Integer errorStatus;

    /** 错误次数, 数据库字段：count */
    private Integer count;

    /**  数据库字段：is_deleted */
    @com.gitee.fastmybatis.core.annotation.LogicDelete
    private Byte isDeleted;

    /**  数据库字段：gmt_create */
    private Date gmtCreate;

    /**  数据库字段：gmt_modified */
    private Date gmtModified;
}
